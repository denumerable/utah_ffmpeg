/*
 * internals for UTAH codecs
 * Copyright (c) 2005 Chad Crosby
 *
 * This file is NOT part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef AVCODEC_UTAH_H
#define AVCODEC_UTAH_H

#include "avcodec.h"

typedef struct UTAHContext {
    AVFrame picture;
} UTAHContext;

typedef enum {
    UTAH_RGB         =0,
    UTAH_RLE8        =1,
    UTAH_RLE4        =2,
    UTAH_BITFIELDS   =3,
} BiCompression;

#endif /* AVCODEC_UTAH_H */
